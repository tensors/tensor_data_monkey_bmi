%% Example of running CP

%% Load data
load('data.mat');

%% Convert to tensor
X = tensor(X);

%% Compute nonnegative tensor factorization
rng('default')
M = cp_opt(X,10,'lower',0);
fit = 1-norm(X-full(M))/norm(X);
fprintf('Proportion of data captured by CP: %0.2f\n', fit);
fprintf('Relative error: %0.2f\n', 100*norm(X-full(M))/norm(X))

%% Visualization of data
% The legend option requires legendflex-pkg, available at
% https://github.com/kakearney/legendflex-pkg.  
% To disable the legend, set the last argument to false.
viz_monkey_bmi_cp(M,angle,1,true);

%% Save to file
exportgraphics(gcf,'cp_monkey_bmi.png','Resolution',300)

%% Visualize in two pieces
viz_monkey_bmi_cp(extract(M,1:5),angle,2,true);

%%
exportgraphics(gcf,'cp_monkey_bmi_1.png','Resolution',300)

%% 
viz_monkey_bmi_cp(extract(M,6:10),angle,3,true);

%%
exportgraphics(gcf,'cp_monkey_bmi_2.png','Resolution',300)

%% View 3rd factor plus angles
U3 = M.U{3};
fprintf('| Angle |');
for j = 1:10
    fprintf(' Wgt %d |', j)
end
fprintf('\n');
for i = 1:11
    fprintf('|----');
end
fprintf('|\n');
for i = 1:size(X,3)
    fprintf('| %3d |', angle(i));
    for j = 1:10
        fprintf(' %0.3f |', U3(i,j))
    end
    fprintf('\n');
end


%% Cluster based on 3rd factor
rng('default')
K = length(angle_list);
[cidx,ctrs] = kmeans(M.U{3}.*M.lambda',K,'Replicates',5,...
    'Distance','correlation');
[~,truth] = ismember(angle,angle_list);
C = confusionmat(cidx,truth)

%% View confusion matrix as a table
fprintf('| Cluster | ');
for i = 1:4
    fprintf(' %3d |', angle_list(i));
end
fprintf('\n');

for i = 1:5
    fprintf('|----');
end
fprintf('|\n');

for i = 1:4
    fprintf('| %d |', i);
    for j = 1:4
        fprintf(' %d |', C(i,j))
    end
    fprintf('\n');
end