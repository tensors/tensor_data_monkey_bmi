function h = viz_monkey_bmi_cp(M,angle,figid,lgdfs)
%VIZ_MONKEY_BMI_CP Visualization of CP for Monkey BMI data.
%
%   VIZ_MONKEY_BMI_CP(M,ANGLES) visualizes the components in the Kruskal
%   tensor M which corresponds to the Monkey BMI data tensor of size 
%   43 x 88 x 88. The first mode corresponds to neurons and is plotted as a
%   bar chart, the second mode corresponds to time and is plotted as a
%   line, and the third mode corresponds to trials and is plotted as a
%   scatter plot. The second input argument, ANGLES, is a column vector of
%   angles corresponding to the third mode. In other words, size(ANGLES,1)
%   = size(M,3). This used to color code the scatter plot in mode 3. 
%
%   VIZ_MONKEY_BMI_CP(M,ANGLES,FIGID) specifies the figure ID with FIGID.
%
%   VIZ_MONKEY_BMI_CP(M,ANGLES,FIGID,TRUE) plots the legend for mode 3. 
%   The legend option requires legendflex-pkg, available at
%   https://github.com/kakearney/legendflex-pkg.  
%
%   H = VIZ_MONKEY_BMI_CP(...) returns a set of handles to all the figure
%   elements.
%
%   Requires the following packages
%   - Tensor Toolbox for MATLAB (http://www.tensortoolbox.org/)
%   - L-BFGS-B wrapper (https://github.com/stephenbeckr/L-BFGS-B-C)
%   - LegendFlex (https://github.com/kakearney/legendflex-pkg)
%  
%   For more info: https://gitlab.com/tensors/tensor_data_monkey_bmi

%Tammy Kolda, tammy.kolda@mathsci.ai, 2021.

%%

% Create a new figure if no figure id is provided
if ~exist('figid','var')
    fh = figure;
    figid = fh.Number;
end

% Legend off by default
if ~exist('lgdfs','var')
    lgdfs = false;
end

% Number of components in model
r = ncomponents(M);


% Set some colors
C = colormap('lines');

% Set height parameters (absolute pixels)
toppx = 25;
btmpx = 35;
factpx = 90;
hspx = 3;
hpx = r*factpx + (r-1)*hspx + toppx + btmpx;

% Set width parameters (absolute pixels)
ltpx = 40;
if lgdfs
    rtpx = 50;
else
    rtpx = 5;
end
wspx = 990;
wpx = wspx + ltpx + rtpx;

% Set up plot commands
fbn = @(x,y) bar(x,y,0.5,'FaceColor',C(5,:),'EdgeColor',C(5,:)*0.5);
fpt = @(x,y) plot(x,y,'Color',C(6,:),'Linewidth',2);
fpa = @(x,y) plot_angle(x,y,angle,lgdfs);

% Call viz, with absolute sizes set to relative per the way that it's
% called.
h = viz(M, 'PlotCommands', {fbn,fpt,fpa}, 'FactorTitles','true weight',...
    'ModeTitles',{'Neuron','Time','Trial'},'Figure',figid,...
    'ShowZero',[false,true,false],'RelModeWidth',[1.5 1 2.5 ],...
    'LeftSpace',ltpx/wpx,'RightSpace',rtpx/wpx,...
    'TopSpace',toppx/hpx,'BottomSpace',btmpx/hpx,'HorzSpace',hspx/hpx,...
    'BaseFontSize',14);

% Fix Xticks on middle plot so that they don't overrun those on the
% rightmost one.
set(h.FactorAxes(2,r),'XTick',0:50:150);

% Resize (keeping position of upper left corner the same)
width = wpx/100;     % Width in inches
height = hpx/100;    % Height in inches
pos = get(gcf, 'Position');
delta_y = max(0, height*100 - pos(4));
set(gcf, 'Position', [pos(1) (pos(2)-delta_y) width*100, height*100]); 


end
%%
function h = plot_angle(xx,yy,aa,lgdfs)

%% Set some colors
C1 = colormap('lines');
C2 = colormap('colorcube');
mycolors = [C1(1:7,:); C2(1,:)];
clear C1 C2

%%
aa_unique = unique(aa);
h = cell(length(aa_unique),1);
for i = 1:length(aa_unique)
    idx = find(aa == aa_unique(i));
    h{i} = scatter(xx(idx),yy(idx),20,'filled','MarkerFaceColor',mycolors(i,:));
    hold on;
end
hold off
if lgdfs 
    lgd = cellstr(num2str(aa_unique));
    legendflex(lgd,'anchor',{'ne','nw'},'buffer',[3 0],...
        'xscale',0.3,'fontsize',12,'padding',[0 0 1],'nolisten',true);
end

end

