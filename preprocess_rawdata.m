%% Filter, clean, and smooth original data 
% This script converts rawdata.mat to data.mat.
% It also creates rawdatamat.mat which stores the mapping between the new
% data in data.mat and the original data in rawdata.mat.

%% Data source information
% This data was obtained courtesy of Alex Williams.

%% Load raw data 
% This file contains all the data originally provided by Stanford, but not
% all of it is actually used.
clear
rawdata = load('rawdata.mat');

%% Extract data we need, transposing some
angle = rawdata.angle';
xloc = rawdata.x';
yloc = rawdata.y';
X = rawdata.tensor;
savetag = rawdata.savetag;
first_acquire = rawdata.first_acquire;
target = rawdata.target;

%% Filter experiments
% * savetag == 5 -> no shift in orientation
% * first_acquire < 600 -> have at least 500 ms of 'hold' data and remove
%   excessively long times to acquire target
% * keep only 4 possible angles (out of 8 originally)
k_orig = find((savetag == 5) & (first_acquire<600) & ismember(angle',[-90 90 0 180]))';
X = X(:,:,k_orig);
angle = angle(k_orig);
first_acquire = first_acquire(k_orig);
target = target(k_orig,:);
xloc = xloc(k_orig);
yloc = yloc(k_orig);

%% Remove less active neurons and sort from greatest to least activity
iitotal = double(collapse(tensor(X),-1, @sum));
[iitotal,isrt] = sort(iitotal,'descend');
i_select = find(iitotal > 500);
i_orig = isrt(i_select); 
X = X(i_orig,:,:);

%% Extract and save angle list
[angle_list,loc] = unique(angle);
angle_xyloc = target(loc,:);

%% Adjust first_acquire by subtracting 20
% For some reason, this is off by exactly 20ms
first_acq = first_acquire'-20;

%% Remap the times for each experiment
E = size(X,3);
T1 = 100; % Alignment for first_acquire, with a median value of 496
T2 = 100; % Alignment for extra 500 ms
HT = 500;
T = T1+T2;
fa = first_acq;

timewarp = cell(E,1);
for k = 1:E
    
    newtime1 = T1*(1:fa(k))./fa(k);
    newtime2 = linspace(T1+0.5,T,HT);
    timewarp{k} = [newtime1 newtime2];
    
end

%% Do smoothing and interpolation of the X values
N = size(X,1);
Xnew = zeros([N T E]);
endtime = first_acq + HT;
loc = zeros([2 T E]);

for i = 1:N
    for k = 1:E
        fprintf('experiment=%d,neuron=%d\n',k,i);
        xx = smoothdata(X(i,1:endtime(k),k),'gaussian',300);
        Xnew(i,:,k) = interp1(timewarp{k},xx,1:T);
        loc(1,:,k) = interp1(timewarp{k},xloc{k}(1:endtime(k)),1:T);
        loc(2,:,k) = interp1(timewarp{k},yloc{k}(1:endtime(k)),1:T);
    end
end
Xold = X;
X = Xnew; 
clear Xnew;


%% Save relevant data
save('data.mat','X','loc','angle','angle_list','angle_xyloc');
save('rawdatamap.mat','k_orig','i_orig');