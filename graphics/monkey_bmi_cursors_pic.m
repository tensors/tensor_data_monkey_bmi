%% Script to create picture of cursor movements
% Creates the image monkey_bmi_cursors.png which shows the cursor
% movements. This should be run from within the graphics directory.

%% Load data
load ../data.mat

%% Plot everything in one massive figure
figure(1); clf; hold on

% Plot boxes around targets
boxcolor = 0.5*[1 1 1];
% Note that we need to go past the last corner in order that it is properly
% rounded in the image, this is why we redraw the eastern edge.
mybox = [1 1 ; 1 -1; -1 -1; -1 1; 1 1; 1 -1];
startbox = 25*mybox;
h = plot(startbox(:,1), startbox(:,2),'LineWidth',4,'Color',boxcolor);
for i = 1:4
    tbox = 20*mybox - angle_xyloc(i,:);
    plot(tbox(:,1), tbox(:,2),'LineWidth',4,'Color',boxcolor);
end

% Set some limits
xlim([-125 125])
ylim([-110 140])

% Set some colors
C1 = colormap('lines');
C2 = colormap('colorcube');
C = [C1(1:7,:); C2(1,:)];

for i = 1:length(angle_list)
    
    % Set the color
    c = C(i,:);
    
    % Find the experiments with that angle
    expset = find(angle == angle_list(i));
    for k = expset'
        xx = loc(1,:,k);
        yy = loc(2,:,k);
        plot(xx(1:100),yy(1:100),'-','Color',c);        
        plot(xx(100),yy(100),'.','Color',c,'MarkerSize',20);        
        plot(xx(100:end),yy(100:end),'--','Color',c);        
    end
     
end

% Re-plot targets
htext=text(angle_xyloc(:,1),angle_xyloc(:,2),num2str(angle_list,'%d'),'HorizontalAlignment','center','FontSize',20,'FontWeight','bold');

% Plot starting point
%plot(0,0,'ko','MarkerSize',20,'LineWidth',3,'Color',boxcolor);

%%
axis equal
xlim([-125 125])
ylim([-110 140])
axis off
axis image


%% Export

width = 6;     % Width in inches
height = 6;    % Height in inches
pos = get(gcf, 'Position');
delta_y = max(0, height*100 - pos(4));
set(gcf, 'Position', [pos(1) (pos(2)-delta_y) width*100, height*100]); %<- Set size

%%

exportgraphics(gca,'monkey_bmi_cursors.png','Resolution',300)

