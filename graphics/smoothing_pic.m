%% Show some preprocessed data
% Run this right after the preprocessing script. Run the preprocessing
% script in the main directory, then cd into graphics and run this script,
% repeating with different values of k and i as needed.

%%
figure(3); clf;
k = 36; %exp
i = 1; %neuron
mm = 2;
nn = 1;
subplot(mm,nn,1)
bar(double(Xold(i,1:endtime(k),k)))
hold on;
plot([first_acq(k) first_acq(k)],ylim,'r--','LineWidth',2)
title(sprintf('Raw Data Spikes (Exp. %d, Neuron %d)',k,i))
xlabel('Time (ms)')
subplot(mm,nn,2)
plot(double(X(i,:,k)))
title(sprintf('Smoothed and Aligned Data (Exp. %d, Neuron %d)',k,i))
hold on;
plot([100 100],ylim,'r--','LineWidth',2)
xlabel('Time Step')
fname = sprintf('smoothing-pic-%d-%d',k,i);
print(fname,'-dpng','-r300')