%% Visualize the neurons
% Creates the image monkey_bmi_neurons.png which shows the cursor
% movements. This should be run from within the graphics directory.

%% Set some colors
C1 = colormap('lines');
C2 = colormap('colorcube');
C = [C1(1:7,:); C2(1,:)];
clear C1 C2

%% Loda data
load('../data.mat');


%% Plot some data

% Determine number of subplots
MM = 3;
NN = 3;
PP = MM*NN;

idx = [1 2 3 4 5 6 11 16 17];

Y = X(idx,:,:);
ymax = max(Y(:));
figure(4); clf;


for p = 1:min(PP,length(idx))
    ii = idx(p);
    subplot(MM,NN,p);
    for j = 1:length(angle_list)
        explist = find(angle == angle_list(j))';
        for k = explist
            plot(X(ii,:,k), 'Color', C(j,:),'LineWidth',0.1);
            hold on;
        end
    end
    for j = 1:length(angle_list)
        explist = find(angle == angle_list(j))';
        Y = squeeze(X(ii,:,explist));
        y = mean(Y');
        h(j) = plot(y, 'Color', C(j,:),'LineWidth',3);
        lgd{j} = sprintf('Angle %d',angle_list(j));
    end
    
    plot([100 100],[0 ymax],'k--');
    title(sprintf('Neuron %d', ii));
    ylim([0 ymax])
    xlim([0 size(X,2)])
    legend(h,lgd);
    
end


%% Resize
width = 12;
height = 9;
pos = get(gcf, 'Position');
delta_y = max(0, height*100 - pos(4));
set(gcf, 'Position', [pos(1) (pos(2)-delta_y) width*100, height*100]); %<- Set size

%% Save to file

exportgraphics(gcf,'monkey_bmi_neurons.png','Resolution',300)