# Monkey BMI Tensor Dataset

## How to Cite

* T. G. Kolda, Monkey BMI Tensor Dataset, https://gitlab.com/tensors/tensor_data_monkey_bmi, 2021.
* S. Vyas, N. Even-Chen, S. D. Stavisky, S. I. Ryu, P. Nuyujukian, and K. V. Shenoy, Neural Population Dynamics Underlying Motor Learning Transfer, Elsevier BV, Vol. 97, No. 5, pp. 1177-1186.e3, March 2018, https://doi.org/10.1016/j.neuron.2018.01.040.
* S. Vyas, D. J. O'Shea, S. I. Ryu, and K. V. Shenoy, Causal Role of Motor Preparation during Error-Driven Learning, Neuron, Elsevier BV, Vol. 106, No. 2, pp. 329-339.e4, April 2020, https://doi.org/10.1016/j.neuron.2020.01.019.

This dataset has also been used in the context of tensor analysis in the following paper:

* A. H. Williams, T. H. Kim, F. Wang, S. Vyas, S. I. Ryu, K. V. Shenoy, M. Schnitzer, T. G. Kolda, S. Ganguli, Unsupervised Discovery of Demixed, Low-dimensional Neural Dynamics across Multiple Timescales through Tensor Components Analysis, Neuron, 98(6):1099-1115, 2018, https://doi.org/10.1016/j.neuron.2018.05.015.


## Description

The **Monkey Brain Machine Interface (BMI)** tensor data has been curated from a series of experiments as reported 
in Vyas et al. (2018), Vyas et al. (2020), and Williams et al. (2018).
In each experiment, a monkey moves a cursor to one of four targets (at 0, 90, 180, and -90 degrees) using a brain machine interface (BMI).
The actual cursor path trajectories are shown below in the image on the right. Each trajectory corresponds to a single experiment and
is color coded by the target. The cursors starts in the box in the center and must move towards a target box. We mark a circle at the point in time when the target is *acquired*. Then the line becomes dashed, while the  cursor is held at the target for 500ms.

<img src="graphics/monkey_bmi_graphic.png" width="300px" alt="Illustration of single trial. Image adapted from Williams et al. (2018)">   
<img src="graphics/monkey_bmi_cursors.png" width="350px" alt="Cursor paths for 88 trials.">

The tensor is formatted as 
**43 neurons x 200 time steps x 88 trials**.
The data has been preprecessed to align the target acquistion time and smooth the neuron spike data 
(see [below](#source-and-preprocessing) for details).

We also have additional metadata on the movement of the cursor for each trial as shown below. We show the trajectories of every experiment. The cursor starts in the box in the center and must move towards a target box. We mark a circle at the point in time when the target is *acquired* (corresponding to time step 100 in our aligned data). Then the line becomes dashed, and the monkey must hold the cursor at the target for 500ms (corresponding to 100 times steps in our aligned data).

## Data and Main Files

### Variables

The file `data.mat` is the main data file containing the Monkey BMI tensor.

* `X` - Monkey BMI tensor of size 43 x 200 x 88, nonnegative. Note that this is stored as a MATLAB multidimensional array (MDA) and needs to be converted to a Tensor Toolbox tensor via `X = tensor(X)`.
* `angle` - Array of length 88 with angles identifying target for each experiment.
* `angle_list` - List of unique angles: [-90, 0, 90, 180].
* `angle_xyloc` - Corresponding (x,y) coordinate for each angle.
* `loc` - Multi-dimensional array of size 2 x 200 x 88 such that `loc(:,t,k)` is the (x,y) coordinate for experiment `k` at time step `t`.

The number of experiments for each angle is given in the table below


| Angle | Count |
|-------|-------|
| -90 | 19 |
| 0 | 20 |
| 90 | 28 |
| 180 | 21 |



### Main Files

The main files of interest are listed here. All other files are [listed below](#other-files).

* `data.mat` - Main data file (see descriptions in [Variables](#variables))
* `cp_monkey_bmi_example.m` - Example of running CP on the Monkey BMI tensor 
* `viz_monkey_bmi_cp.m` - Visualization of CP factors of the Monkey BMI tensor

## Visualization of the Monkey BMI Tensor

We visualize the activities of the several neurons in the figure below. Each neuron corresponds to a tensor frontal slice. For instance, the figure in the top left corresponds to neuron 1, so it is represented by `X(1,:,:)`. Each line corresponds to a single trial, so each line is represented by a row fiber `X(1,:,k)` for `k` ranging from 1 to 88. The lines are color coded by the angle used in the experiment (metadata). The thick line is the mean for each angle.

<img src="graphics/monkey_bmi_neurons.png" alt="Visualization of tensor fibers">

## CP Computing, Visualization, and Clustering

We give an example of using the [Tensor Toolbox for MATLAB](https://www.tensortoolbox.org/) to compute the CP decomposition of the tensor. The code is available in the file `cp_monkey_bmi_example.m`. 

### Computing CP Tensor Decomposition

We can compute a CP decomposition of the data tensor. 
Note that `X` is stored a multidimensional array (MDA) so that it is useful even for persons not using the Tensor Toolbox for MATLAB; hence, we must first convert it.
We then compute a rank-10 nonnegative CP decomposition using the `cp_opt` command.
For reproducibility, we call `rng` and set it to its default setting.
The resulting CP decomposition describes 65\% of the data tensor.

```matlab
%% Load data
load('data.mat');

%% Convert to tensor
X = tensor(X);

%% Compute nonnegative tensor factorization
rng('default')
M = cp_opt(X,10,'lower',0);
fit = 1-norm(X-full(M))/norm(X);
fprintf('Proportion of data captured by CP: %0.2f\n', fit);

```

### Visualizing Factors

We provide a method for visualizing the CP factors. This calls the `ktensors/viz` method in the Tensor Toolbox, but with some enhancements specific to this data, inclusing color-coding the experiments by the angle. 

```matlab
%% Visualization of data
viz_monkey_bmi_cp(M,angle,1,true);

%% Save to file
exportgraphics(gcf,'cp_monkey_bmi.png','Resolution',300)
```

We show the results in the image below.
Each row corresponds to a rank-one components.
The weights to the left give the relative weight of each component, proportional to the first.
Each column corresponds to a mode of the tensor.
We plot mode 1 as a bar chart, showing the activity of each neuron for this factor.
We plot mode 2 as a line, showing the activity over time for this factor.
We plot mode 3 as a scatter plot, color-coded by target, showing which targets are most active for this factor.
Observe how the yellow dots in Component 1 are separated from the rest, so this component seems to 
be most important to the 90-degree target. Component 4 is similar for purple, component 6 for orange,
and component 7 for blue. None is quite perfect, but you get some sense that it picks up on the differences between the different angles.

<img src="cp_monkey_bmi.png" alt="Visualization of CP tensor decomposition">

### Clustering

The mode-3 factor matrix can be used for clustering. First, we present the matrix,
augmented by the angle for each trial. 

| Angle | Wgt 1 | Wgt 2 | Wgt 3 | Wgt 4 | Wgt 5 | Wgt 6 | Wgt 7 | Wgt 8 | Wgt 9 | Wgt 10 |
|----|----|----|----|----|----|----|----|----|----|----|
| 180 | 0.011 | 0.179 | 0.111 | 0.190 | 0.020 | 0.033 | 0.011 | 0.072 | 0.199 | 0.037 |
|   0 | 0.036 | 0.023 | 0.083 | 0.017 | 0.125 | 0.180 | 0.000 | 0.065 | 0.014 | 0.000 |
| -90 | 0.003 | 0.000 | 0.134 | 0.037 | 0.081 | 0.040 | 0.162 | 0.064 | 0.021 | 0.143 |
|  90 | 0.209 | 0.168 | 0.177 | 0.071 | 0.005 | 0.039 | 0.000 | 0.208 | 0.098 | 0.078 |
|   0 | 0.046 | 0.036 | 0.139 | 0.011 | 0.081 | 0.180 | 0.054 | 0.126 | 0.000 | 0.049 |
| 180 | 0.049 | 0.117 | 0.155 | 0.238 | 0.043 | 0.000 | 0.000 | 0.025 | 0.170 | 0.078 |
| -90 | 0.000 | 0.005 | 0.133 | 0.036 | 0.092 | 0.035 | 0.156 | 0.066 | 0.041 | 0.147 |
|  90 | 0.205 | 0.159 | 0.175 | 0.046 | 0.107 | 0.069 | 0.037 | 0.062 | 0.000 | 0.159 |
| 180 | 0.041 | 0.157 | 0.096 | 0.107 | 0.064 | 0.017 | 0.108 | 0.020 | 0.139 | 0.185 |
| -90 | 0.000 | 0.000 | 0.095 | 0.047 | 0.077 | 0.026 | 0.182 | 0.115 | 0.080 | 0.026 |
|   0 | 0.052 | 0.030 | 0.114 | 0.028 | 0.069 | 0.206 | 0.000 | 0.172 | 0.015 | 0.011 |
|  90 | 0.125 | 0.138 | 0.158 | 0.022 | 0.084 | 0.061 | 0.059 | 0.047 | 0.053 | 0.174 |
|   0 | 0.022 | 0.023 | 0.122 | 0.038 | 0.088 | 0.230 | 0.000 | 0.137 | 0.021 | 0.100 |
| 180 | 0.000 | 0.171 | 0.000 | 0.181 | 0.142 | 0.000 | 0.190 | 0.024 | 0.248 | 0.228 |
|  90 | 0.215 | 0.123 | 0.152 | 0.008 | 0.085 | 0.000 | 0.081 | 0.077 | 0.084 | 0.083 |
|  90 | 0.127 | 0.111 | 0.154 | 0.047 | 0.110 | 0.066 | 0.087 | 0.027 | 0.108 | 0.067 |
|   0 | 0.000 | 0.059 | 0.034 | 0.000 | 0.121 | 0.155 | 0.082 | 0.225 | 0.070 | 0.070 |
|  90 | 0.178 | 0.136 | 0.192 | 0.076 | 0.000 | 0.034 | 0.000 | 0.120 | 0.029 | 0.150 |
| 180 | 0.000 | 0.158 | 0.034 | 0.258 | 0.073 | 0.035 | 0.093 | 0.015 | 0.290 | 0.169 |
|   0 | 0.000 | 0.000 | 0.079 | 0.000 | 0.243 | 0.215 | 0.063 | 0.090 | 0.046 | 0.015 |
| -90 | 0.003 | 0.000 | 0.095 | 0.028 | 0.104 | 0.048 | 0.190 | 0.104 | 0.066 | 0.013 |
|  90 | 0.198 | 0.114 | 0.106 | 0.000 | 0.166 | 0.071 | 0.035 | 0.082 | 0.047 | 0.171 |
|   0 | 0.002 | 0.030 | 0.105 | 0.008 | 0.117 | 0.189 | 0.065 | 0.050 | 0.008 | 0.231 |
| 180 | 0.000 | 0.152 | 0.001 | 0.173 | 0.114 | 0.000 | 0.187 | 0.000 | 0.276 | 0.123 |
| -90 | 0.000 | 0.056 | 0.068 | 0.000 | 0.143 | 0.077 | 0.189 | 0.022 | 0.046 | 0.294 |
|  90 | 0.215 | 0.115 | 0.168 | 0.047 | 0.042 | 0.067 | 0.025 | 0.128 | 0.010 | 0.125 |
| -90 | 0.000 | 0.025 | 0.070 | 0.055 | 0.121 | 0.000 | 0.214 | 0.066 | 0.062 | 0.078 |
| 180 | 0.033 | 0.191 | 0.104 | 0.187 | 0.080 | 0.000 | 0.057 | 0.020 | 0.122 | 0.023 |
|   0 | 0.026 | 0.000 | 0.085 | 0.016 | 0.138 | 0.247 | 0.028 | 0.073 | 0.007 | 0.042 |
|  90 | 0.190 | 0.109 | 0.105 | 0.109 | 0.083 | 0.058 | 0.000 | 0.074 | 0.068 | 0.086 |
|   0 | 0.019 | 0.024 | 0.041 | 0.024 | 0.152 | 0.272 | 0.047 | 0.019 | 0.014 | 0.113 |
| -90 | 0.000 | 0.000 | 0.078 | 0.000 | 0.117 | 0.041 | 0.140 | 0.020 | 0.047 | 0.081 |
| 180 | 0.041 | 0.021 | 0.013 | 0.128 | 0.144 | 0.000 | 0.114 | 0.000 | 0.151 | 0.055 |
|  90 | 0.180 | 0.093 | 0.119 | 0.077 | 0.062 | 0.071 | 0.038 | 0.175 | 0.026 | 0.059 |
|   0 | 0.035 | 0.012 | 0.032 | 0.044 | 0.110 | 0.261 | 0.031 | 0.000 | 0.083 | 0.054 |
| 180 | 0.000 | 0.168 | 0.044 | 0.143 | 0.085 | 0.023 | 0.135 | 0.091 | 0.150 | 0.109 |
|  90 | 0.131 | 0.165 | 0.095 | 0.099 | 0.098 | 0.050 | 0.045 | 0.023 | 0.137 | 0.000 |
|  90 | 0.203 | 0.141 | 0.172 | 0.014 | 0.102 | 0.016 | 0.050 | 0.071 | 0.003 | 0.023 |
|  90 | 0.196 | 0.101 | 0.158 | 0.016 | 0.064 | 0.075 | 0.000 | 0.094 | 0.007 | 0.206 |
|   0 | 0.008 | 0.045 | 0.056 | 0.009 | 0.092 | 0.149 | 0.073 | 0.210 | 0.026 | 0.000 |
| -90 | 0.000 | 0.052 | 0.100 | 0.111 | 0.086 | 0.000 | 0.175 | 0.021 | 0.084 | 0.078 |
|   0 | 0.060 | 0.080 | 0.142 | 0.036 | 0.117 | 0.234 | 0.000 | 0.252 | 0.011 | 0.000 |
| -90 | 0.000 | 0.050 | 0.061 | 0.052 | 0.093 | 0.000 | 0.193 | 0.175 | 0.125 | 0.000 |
|  90 | 0.214 | 0.110 | 0.177 | 0.064 | 0.024 | 0.063 | 0.000 | 0.106 | 0.000 | 0.178 |
| 180 | 0.000 | 0.151 | 0.078 | 0.288 | 0.023 | 0.023 | 0.052 | 0.086 | 0.223 | 0.000 |
|   0 | 0.037 | 0.019 | 0.114 | 0.012 | 0.140 | 0.205 | 0.007 | 0.190 | 0.000 | 0.000 |
| -90 | 0.000 | 0.039 | 0.012 | 0.060 | 0.162 | 0.029 | 0.266 | 0.142 | 0.083 | 0.000 |
|  90 | 0.230 | 0.169 | 0.158 | 0.119 | 0.011 | 0.034 | 0.024 | 0.065 | 0.063 | 0.000 |
|  90 | 0.182 | 0.121 | 0.199 | 0.067 | 0.028 | 0.013 | 0.035 | 0.097 | 0.008 | 0.058 |
| -90 | 0.000 | 0.023 | 0.074 | 0.049 | 0.116 | 0.009 | 0.161 | 0.130 | 0.037 | 0.108 |
|   0 | 0.042 | 0.026 | 0.079 | 0.022 | 0.119 | 0.169 | 0.028 | 0.161 | 0.000 | 0.119 |
| 180 | 0.018 | 0.145 | 0.056 | 0.256 | 0.069 | 0.000 | 0.039 | 0.000 | 0.169 | 0.068 |
| -90 | 0.000 | 0.036 | 0.035 | 0.000 | 0.089 | 0.044 | 0.183 | 0.271 | 0.076 | 0.011 |
|   0 | 0.018 | 0.000 | 0.051 | 0.013 | 0.174 | 0.179 | 0.119 | 0.038 | 0.063 | 0.000 |
| -90 | 0.000 | 0.000 | 0.085 | 0.000 | 0.159 | 0.047 | 0.159 | 0.139 | 0.045 | 0.113 |
| 180 | 0.000 | 0.101 | 0.030 | 0.225 | 0.095 | 0.002 | 0.124 | 0.079 | 0.209 | 0.065 |
|  90 | 0.188 | 0.175 | 0.108 | 0.055 | 0.064 | 0.064 | 0.049 | 0.062 | 0.051 | 0.141 |
| 180 | 0.036 | 0.182 | 0.078 | 0.181 | 0.081 | 0.049 | 0.062 | 0.024 | 0.180 | 0.056 |
|   0 | 0.002 | 0.006 | 0.061 | 0.038 | 0.178 | 0.208 | 0.045 | 0.110 | 0.000 | 0.000 |
|  90 | 0.190 | 0.049 | 0.147 | 0.104 | 0.096 | 0.086 | 0.048 | 0.000 | 0.016 | 0.059 |
| -90 | 0.000 | 0.012 | 0.088 | 0.066 | 0.071 | 0.008 | 0.159 | 0.023 | 0.072 | 0.008 |
|  90 | 0.133 | 0.083 | 0.135 | 0.000 | 0.168 | 0.084 | 0.092 | 0.156 | 0.000 | 0.000 |
| 180 | 0.077 | 0.116 | 0.033 | 0.252 | 0.036 | 0.008 | 0.080 | 0.003 | 0.239 | 0.117 |
| 180 | 0.077 | 0.164 | 0.087 | 0.144 | 0.083 | 0.003 | 0.110 | 0.000 | 0.145 | 0.203 |
|   0 | 0.000 | 0.058 | 0.040 | 0.015 | 0.166 | 0.237 | 0.093 | 0.018 | 0.012 | 0.062 |
|  90 | 0.177 | 0.132 | 0.160 | 0.000 | 0.086 | 0.048 | 0.102 | 0.054 | 0.000 | 0.040 |
| -90 | 0.000 | 0.000 | 0.010 | 0.000 | 0.167 | 0.073 | 0.243 | 0.000 | 0.078 | 0.121 |
| -90 | 0.000 | 0.011 | 0.101 | 0.000 | 0.137 | 0.034 | 0.116 | 0.085 | 0.034 | 0.011 |
|   0 | 0.013 | 0.030 | 0.038 | 0.000 | 0.134 | 0.193 | 0.106 | 0.000 | 0.019 | 0.122 |
|  90 | 0.229 | 0.086 | 0.140 | 0.009 | 0.109 | 0.057 | 0.033 | 0.134 | 0.000 | 0.175 |
| -90 | 0.000 | 0.015 | 0.061 | 0.039 | 0.085 | 0.035 | 0.167 | 0.273 | 0.055 | 0.000 |
|  90 | 0.164 | 0.150 | 0.179 | 0.042 | 0.046 | 0.017 | 0.051 | 0.123 | 0.030 | 0.031 |
|  90 | 0.191 | 0.097 | 0.138 | 0.055 | 0.077 | 0.075 | 0.047 | 0.007 | 0.000 | 0.209 |
| 180 | 0.039 | 0.168 | 0.109 | 0.206 | 0.037 | 0.007 | 0.061 | 0.035 | 0.105 | 0.024 |
| 180 | 0.050 | 0.246 | 0.088 | 0.127 | 0.037 | 0.000 | 0.088 | 0.092 | 0.157 | 0.000 |
|  90 | 0.133 | 0.106 | 0.092 | 0.048 | 0.090 | 0.076 | 0.037 | 0.035 | 0.016 | 0.089 |
| -90 | 0.012 | 0.045 | 0.043 | 0.000 | 0.137 | 0.021 | 0.189 | 0.231 | 0.057 | 0.000 |
| 180 | 0.026 | 0.106 | 0.033 | 0.233 | 0.093 | 0.004 | 0.033 | 0.007 | 0.296 | 0.190 |
| -90 | 0.000 | 0.000 | 0.088 | 0.000 | 0.112 | 0.032 | 0.194 | 0.115 | 0.036 | 0.239 |
|   0 | 0.050 | 0.018 | 0.016 | 0.028 | 0.159 | 0.186 | 0.042 | 0.089 | 0.076 | 0.151 |
|  90 | 0.219 | 0.153 | 0.139 | 0.000 | 0.092 | 0.051 | 0.112 | 0.111 | 0.000 | 0.094 |
| 180 | 0.052 | 0.187 | 0.115 | 0.196 | 0.044 | 0.000 | 0.040 | 0.098 | 0.154 | 0.000 |
|  90 | 0.165 | 0.062 | 0.134 | 0.093 | 0.094 | 0.044 | 0.114 | 0.021 | 0.023 | 0.013 |
| 180 | 0.000 | 0.146 | 0.000 | 0.220 | 0.027 | 0.045 | 0.031 | 0.086 | 0.232 | 0.000 |
|  90 | 0.136 | 0.080 | 0.098 | 0.102 | 0.109 | 0.124 | 0.043 | 0.000 | 0.047 | 0.072 |
|   0 | 0.030 | 0.055 | 0.091 | 0.000 | 0.153 | 0.193 | 0.048 | 0.087 | 0.013 | 0.000 |
| 180 | 0.059 | 0.144 | 0.057 | 0.155 | 0.113 | 0.000 | 0.117 | 0.022 | 0.148 | 0.000 |
|  90 | 0.151 | 0.151 | 0.098 | 0.027 | 0.085 | 0.060 | 0.072 | 0.112 | 0.000 | 0.061 |

The following MATLAB code can be used to do k-means. It requires the Statistics and Machine Learning Toolbox.

```matlab
rng('default')
K = length(angle_list);
[cidx,ctrs] = kmeans(M.U{3}.*M.lambda',K,'Replicates',5,...
    'Distance','correlation');
[~,truth] = ismember(angle,angle_list);
C = confusionmat(cidx,truth)
```
The resulting confusion matrix shows a perfect clustering by angle.

| Cluster |  -90 |   0 |  90 | 180 |
|----|----|----|----|----|
| 1 | 0 | 0 | 28 | 0 |
| 2 | 19 | 0 | 0 | 0 |
| 3 | 0 | 20 | 0 | 0 |
| 4 | 0 | 0 | 0 | 21 |

## Source and Preprocessing

The details below provide information on how the data was obtained and preprocessed.

### Source

The raw data comes from the lab of [Krishna Shenoy, Stanford University](https://shenoy.people.stanford.edu/krishna-shenoy) and [Saurabh Vyas](https://smvyas.github.io/).
It was electrically recorded via [Utah arrays](https://www.brainlatam.com/manufacturers/microelectrodes/utah-array-335).
The raw data is the number of spikes (action potentials) emitted by each neuron in each 1 millisecond time bin. 
Auxillary data gives the position of the cursor at each time.
We obtained the raw data from [Alex Williams](http://alexhwilliams.info/) as a collection of 11 MATLAB data files which have been combined into one file named `rawdata.mat`.
The data summarizes a total of 859 experiments. 
All times are in milliseconds (ms).
In each experiment, a monkey with a BMI moves a mouse cursor to a particular target (there are 8 distinct targets and one lights up in each experiment) and holds it there for at least 500 ms via *thoughts* only. We pick out four of the targets, at [0, 90, 180, -90] degrees.
In 249 experiments, the *canvas* is unrotated and these are our focus.
(The remaining 610 experiments have a *rotated* canvas, but these are ignored for our purposes.)
The timing for each experiment varies (see `trial_length`)
and is split into two components: 
(1) The time before the target is acquired (see `first_acquire`), and 
(2) the hold time in which the mouse hovers around the target (see `hold_time`). 
There are two sets of measurements collected, as follows.

**Location data.** The mouse cursor (x,y) location is recorded at each millisecond. 
The cells `x{k}` and `y{k}` are arrays that store the (x,y)-coordinate data for trial k and match in the length. Their length is equal to `min{1999,trial_length(k)-1}`.

**Neuron data.** Data is collected from 192 neurons as stored in `tensor`. The tensor is arranged as a neuron x time x experiment. Experiments that were less than 2000 seconds are zero-padded.

Here are the 11 MATLAB variables that were provided:

- `angle` - 859 angles from [-135, -90, -45, 0, 45, 90, 135, 180] (duplicative with `target`)
- `direction` - 'out' repeated 859 times (ignored)
- `first_acquire` - 859 integer values ranging from 330-6248, with one NaN *(Note that this is appears to be off by 20 ms when plotting the mouse movement, so we have adjusted this in our preprocessing.)*
- `hold_time` - 859 integer values ranging from 501-10513, with one NaN
- `savetag` - 859 values from [5 7 9], indicating canvas rotation. We use only `savetag==5` which corresponds to an unrotated canvas.
- `t_maxV` - 859 integers between 20 and 8034 (ignored)
- `target` - 859x2 array with (x,y) coordinates of target (only 8 distinct coordinates)
- `tensor`- 192 x 2000 x 859 tensor of neuron firing readings
- `trial_length` - 859 integers from 869 to 11503 (sum if `first_acquire` and `hold_time`
- `x` - 859 cell array of cursor x-positions of length up to 1999
- `y` - 859 cell array of cursor y-positions of length up to 1999

### Preprocessing 

The script `preprocess_rawdata.m` (run in MATLAB 2020b) was used to convert the file `rawdata.mat` to `data.mat`. The processing is described below, along with some images to show the process.

#### Filtering

The first step in the data preparationg is just filtering, cleaning, and renaming the data.
We make the following changes...

1) Keep only the 88 trials corresponding to `savetag==5` which corresponds to no rotation of the canvas, `first_acquire < 600` (to omit wandering paths and have at least 500 ms of hold time), and `ismember(angle,[-90,0,90,180])` (considering only a subset of all the angles). 
2) Reduce the number of neurons from 192 to 43, retaining only those neurons which fire at least 500 times across all times and all experiments.
3) Reorder the neurons by number of fires (greatest to least).
3) Adjust the `first_acquire` time which is off by 20 seconds.


#### Smoothing and Aligning

Consider a single experiment: We have spike data from 43 neurons, a *first acquire* time (`f`), and a *total* time(`t`). 
It it always the case that `t > f + 500`.
Let `z` be an array of length `t` that corresponds to a single neuron.
We smooth the data using the matlab command `newz = smoothdata(z(1:f+500), 'gaussian', 300)`. 
We then remap the time from `1:f` to 1:100. Likewise, we remap the time from `f+1:f+500` to 101:200.
This yields the smoothed and preprocessed data.

We provide a few examples of the smoothing and aligning in the examples below. Note the difference in the y-scaling on the smoothed data.
The red line denotes the "first acquire" time which is mapped to time step 100.

<img src="graphics/smoothing-pic-10-40.png" height="200px">
<img src="graphics/smoothing-pic-5-17.png" height="200px">
<img src="graphics/smoothing-pic-28-2.png" height="200px">
<img src="graphics/smoothing-pic-36-1.png" height="200px">


### Other Files 

* `rawdata.mat` - Original data (described in detail under [Source](#source))
* `rawdatamap.mat` - Mapping from raw data to data used here
* `preprocess_rawdata.m` - Create `data.mat` and `rawdatamap.mat` (see [Preprocessing](#preprocessing))
* `graphics/monkey_bmi_cursors.png` - Graphic used above, created by `graphics/monkey_bmi_cursors_pic.m`
* `graphics/monkey_bmi_cursors_pic.m` - Creates `graphics/monkey_bmi_cursors.png`
* `graphics/monkey_bmi_graphic.png` - Graphic used above, adapted from Williams et al. (2018)
* `graphics/monkey_bmi_neurons.png` - Graphic used above, created by `graphics/monkey_bmi_neurons_pic.m`
* `graphics/monkey_bmi_neurons_pic.m` - Creates `graphics/monkey_bmi_neurons.png`
* `graphics/smoothing_pic.m` - Creates the pictures `graphics/smoothing-pic-<TRIAL>-<NEURON>.png`
* `graphics/smoothing-pic-<TRIAL>-<NEURON>.png` - Graphics used above, created by `graphics/smoothing_pic.m`
